<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new \Nette\Configurator();
$configurator->setTempDirectory(__DIR__ . '/../runtime/temp');

$configurator->enableDebugger(__DIR__ . '/../runtime/log');
$configurator->createRobotLoader()->addDirectory(__DIR__)->register();

$configurator->addConfig(__DIR__ . '/Config/config.neon', $configurator::AUTO);
if (file_exists($localConf = __DIR__ . '/Config/config.local.neon')) {
	$configurator->addConfig($localConf, $configurator::NONE);
}

$configurator->addParameters([
	'appDir' => __DIR__,
	'wwwDir' => __DIR__ . '/../www'
]);

return $configurator->createContainer();
