<?php



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class FunctionalExampleCest
{

	public function homepage(FunctionalTester $I)
	{
		$I->wantTo('Test homepage');
		$I->amOnPage('/');
		$I->see('Nadpis');
	}

}
