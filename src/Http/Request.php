<?php

namespace Atreo\Codeception\Http;

use Nette\Http\IRequest;
use Nette\Http\Request as HttpRequest;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Request extends HttpRequest implements IRequest
{

	/**
	 * @var RequestFactory
	 */
	private $factory;

	/**
	 * @var HttpRequest
	 */
	private $request;



	public function __construct(\Nette\Http\RequestFactory $factory)
	{
		$this->factory = $factory;
		$this->reset();
	}



	/**
	 * @return HttpRequest
	 */
	public function getRequest()
	{
		return $this->request;
	}



	public function reset()
	{
		$this->request = $this->factory->createHttpRequest();
		$url = $this->request->getUrl();

		if (!$url->getPort()) {
			$url->setPort(80);
		}
	}



	public function getCookie($key, $default = NULL)
	{
		return $this->request->getCookie($key, $default);
	}



	public function getCookies()
	{
		return $this->request->getCookies();
	}



	public function getFile($key)
	{
		return call_user_func_array([$this->request, 'getFile'], func_get_args());
	}



	public function getFiles()
	{
		return $this->request->getFiles();
	}



	public function getHeader($header, $default = NULL)
	{
		return $this->request->getHeader($header, $default);
	}



	public function getHeaders()
	{
		return $this->request->getHeaders();
	}



	public function getMethod()
	{
		return $this->request->getMethod();
	}



	public function getPost($key = NULL, $default = NULL)
	{
		return call_user_func_array([$this->request, 'getPost'], func_get_args());
	}



	public function getQuery($key = NULL, $default = NULL)
	{
		return call_user_func_array([$this->request, 'getQuery'], func_get_args());
	}



	public function getRawBody()
	{
		return $this->request->getRawBody();
	}



	public function getRemoteAddress()
	{
		return $this->request->getRemoteAddress();
	}



	public function getRemoteHost()
	{
		return $this->request->getRemoteHost();
	}



	public function getUrl()
	{
		return $this->request->getUrl();
	}



	public function isAjax()
	{
		return $this->request->isAjax();
	}



	public function isMethod($method)
	{
		return $this->request->isMethod($method);
	}



	public function isSecured()
	{
		return $this->request->isSecured();
	}

}
