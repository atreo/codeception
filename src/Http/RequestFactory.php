<?php

namespace Atreo\Codeception\Http;

use Nette\Http\UrlScript;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class RequestFactory extends \Nette\Http\RequestFactory
{

	/**
	 * @var UrlScript
	 */
	private $fakeUrl;

	/**
	 * @var bool
	 */
	public $isInTest = FALSE;



	/**
	 * @param string|UrlScript $url
	 */
	public function setFakeRequestUrl($url)
	{
		$this->fakeUrl = $url ? new UrlScript($url) : NULL;
	}



	/**
	 * @return Request
	 */
	public function createHttpRequest()
	{
		$isCodeception = isset($_SERVER['argv'][0]) && $_SERVER['argv'][0] == 'vendor/bin/codecept';
		if ($isCodeception || $this->fakeUrl === NULL || PHP_SAPI !== 'cli' || !empty($_SERVER['REMOTE_HOST'])) {
			return parent::createHttpRequest();
		}

		return new \Nette\Http\Request($this->fakeUrl, NULL, array(), array(), array(), array(), PHP_SAPI, '127.0.0.1', '127.0.0.1');
	}

}
