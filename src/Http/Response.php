<?php

namespace Atreo\Codeception\Http;

use Nette\Http\Response as HttpResponse;
use Nette\Utils\DateTime;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Response extends \Nette\Http\Response
{

	/**
	 * @var array
	 */
	private $headers = [];

	/**
	 * @var boolean
	 */
	private $isCodeception;



	public function __construct()
	{
		parent::__construct();
		$this->isCodeception = isset($_SERVER['argv'][0]) && $_SERVER['argv'][0] == 'vendor/bin/codecept';
	}



	public function reset()
	{
		$this->setCode(self::S200_OK);
		$this->headers = [];
	}



	/**
	 * @param string $name
	 * @param string $value
	 *
	 * @return self
	 */
	public function setHeader($name, $value)
	{
		if ($this->isCodeception) {
			$this->headers[$name] = $value;
		} else {
			parent::setHeader($name, $value);
		}

		return $this;
	}



	/**
	 * @param string $name
	 * @param string $value
	 *
	 * @return self
	 */
	public function addHeader($name, $value)
	{
		if ($this->isCodeception) {
			$this->headers[$name] = $value;
		} else {
			parent::addHeader($name, $value);
		}

		return $this;
	}



	/**
	 * @param string $type
	 * @param string $charset
	 *
	 * @return self
	 */
	public function setContentType($type, $charset = NULL)
	{
		$this->setHeader('Content-Type', $type . ($charset ? '; charset=' . $charset : ''));
		return $this;
	}



	/**
	 * @param string $url
	 * @param int $code
	 */
	public function redirect($url, $code = self::S302_FOUND)
	{
		$this->setCode($code);
		$this->setHeader('Location', $url);
	}



	/**
	 * @param string|int|DateTime $time
	 *
	 * @return self
	 */
	public function setExpiration($time)
	{
		if (!$time) {
			$this->setHeader('Cache-Control', 's-maxage=0, max-age=0, must-revalidate');
			$this->setHeader('Expires', 'Mon, 23 Jan 1978 10:00:00 GMT');

			return $this;
		}
		$time = DateTime::from($time);
		$this->setHeader('Cache-Control', 'max-age=' . ($time->format('U') - time()));
		$this->setHeader('Expires', HttpResponse::date($time));

		return $this;
	}



	/**
	 * @return bool
	 */
	public function isSent()
	{
		return FALSE;
	}



	/**
	 * @param string $name
	 * @param mixed $default
	 *
	 * @return mixed
	 */
	public function getHeader($name, $default = NULL)
	{
		if ($this->isCodeception) {
			return isset($this->headers[$name]) ? $this->headers[$name] : $default;
		} else {
			return parent::getHeader($name, $default);
		}
	}



	/**
	 * @return array
	 */
	public function getHeaders()
	{
		if ($this->isCodeception) {
			return $this->headers;
		} else {
			return parent::getHeaders();
		}
	}

}
