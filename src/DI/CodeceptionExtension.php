<?php

namespace Atreo\Codeception\DI;

use Nette\DI\CompilerExtension;
use Nette\Http\IRequest;
use Nette\Http\IResponse;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class CodeceptionExtension extends CompilerExtension
{

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();

		$request = $builder->getByType(IRequest::class);
		$builder->getDefinition($request)
			->setClass('Nette\Http\Request')
			->setFactory('Atreo\Codeception\Http\Request');

		$response = $builder->getByType(IResponse::class);
		$builder->getDefinition($response)
			->setClass('Nette\Http\IResponse')
			->setFactory('Atreo\Codeception\Http\Response');

		$requestFactoryEntity = $builder->getDefinition($builder->getByType('Nette\Http\RequestFactory'))->getFactory()->getEntity();
		if ($requestFactoryEntity == 'Kdyby\Console\HttpRequestFactory') {
			$builder->getDefinition($builder->getByType('Nette\Http\RequestFactory'))
				->setFactory('Atreo\Codeception\Http\RequestFactory');
		}

	}

}
