<?php

namespace Atreo\Codeception\Module;

use Codeception\Step;
use Codeception\TestCase;
use Codeception\TestInterface;
use Nette\Application\Application;
use Nette\Http\Session;
use Nette\Security\User;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Nette extends NetteContainer
{

	/**
	 * @var array
	 */
	protected $config = [
		'bootstrap' => 'app/bootstrap.php',
		'url' => 'http://localhost',
		'followRedirects' => FALSE,
	];

	/**
	 * @var Session
	 */
	private $session;



	public function _initialize()
	{
		parent::_initialize();

		/** @var Session $session */
		$this->session = $this->container->getByType(Session::class);
		$this->session->start();

		Application::$maxLoop = 10000;
	}



	public function _before(TestInterface $test)
	{
		$this->client = new Connector();
		$this->client->setContainer($this->container);
		$this->client->setUrl($this->config['url']);
		$this->client->followRedirects($this->config['followRedirects']);
		parent::_before($test);
	}



	protected function getInternalDomains()
	{
		$domains = parent::getInternalDomains();
		$domain = str_replace('https://', '', $this->config['url']);
		$domain = str_replace('http://', '', $domain);
		$domains[] = '/^' . preg_quote($domain, '/') . '.*$/';
		return $domains;
	}



	public function _afterStep(Step $step)
	{
		parent::_afterStep($step);

		if (class_exists('Kdyby\Doctrine\EntityManager')) {
			/** @var \Kdyby\Doctrine\EntityManager $em */
			$em = $this->container->getByType('Kdyby\Doctrine\EntityManager');
			$em->clear();
		}
	}



	public function _after(TestInterface $test)
	{
		parent::_after($test);

		$this->session->destroy();
		$_SESSION = $_GET = $_POST = $_FILES = $_COOKIE = [];
		$this->session->start();
		$this->container->getByType(User::class)->logout();
	}



	/**
	 * ``` php
	 * <?php
	 * $I->amOnRoute('Front:Homepage');
	 * $I->amOnRoute('Admin:Category:Edit', array('id' => 34));
	 * ?>
	 * ```
	 *
	 * @param $name
	 * @param array $params
	 */
	public function amOnRoute($name, array $params = [])
	{
		$url = $this->client->getUrl($name, $params);
		$this->amOnPage($url);
	}



	public function seeRedirectTo($url)
	{
		if ($this->config['followRedirects']) {
			$this->fail('Method seeRedirectTo only works when followRedirects option is disabled');
		}
		$request = $this->container->getByType('Nette\Http\IRequest');
		$response = $this->container->getByType('Nette\Http\IResponse');
		if ($response->getHeader('Location') !== $request->getUrl()->getHostUrl() . $url && $response->getHeader('Location') !== $url) {
			$this->fail('Couldn\'t confirm redirect target to be "' . $url . '", Location header contains "' . $response->getHeader('Location') . '".');
		}
	}



	public function debugContent()
	{
		$this->debugSection('Content', $this->client->getInternalResponse()->getContent());
	}

}
