<?php

namespace Atreo\Codeception\Module;

use Atreo\Codeception\Http\Request as HttpRequest;
use Atreo\Codeception\Http\Response as HttpResponse;
use Nette\Application\Application;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\Http\Url;
use Nette\Utils\Strings;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class Connector extends Client
{

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @var Application
	 */
	private $application;



	/**
	 * @param Container $container
	 */
	public function setContainer(Container $container)
	{
		$this->container = $container;
		$this->application = $this->container->getByType(Application::class);
		$this->disableCli();
	}



	/**
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}



	/**
	 * @param Request $request
	 * @return \Symfony\Component\BrowserKit\Response
	 * @throws \Exception
	 */
	public function doRequest($request)
	{
		$this->prepareVariables($request);

		$httpRequest = $this->container->getByType(IRequest::class);
		$httpResponse = $this->container->getByType(IResponse::class);
		if (!$httpRequest instanceof HttpRequest || !$httpResponse instanceof HttpResponse) {
			throw new \Exception('Please enable \Atreo\Codeception\DI\CodeceptionExtension.');
		}

		$httpRequest->reset();
		$httpResponse->reset();

		try {
			ob_start();
			$this->application->run();
			$html = ob_get_clean();
		} catch (\Exception $e) {
			ob_end_clean();
			throw $e;
		}

		$code = $httpResponse->getCode();
		$headers = $httpResponse->getHeaders();

		return new Response($html, $code, $headers);
	}



	private function disableCli()
	{
		if (!class_exists('Kdyby\Console\CliRouter')) {
			return;
		}

		foreach ($this->application->getRouter() as $router) {
			if (get_class($router) == 'Kdyby\Console\CliRouter') {
				$router->allowedMethods = [];
			}
		}
	}



	/**
	 * @param $request
	 */
	private function prepareVariables(Request $request)
	{
		$_COOKIE = $request->getCookies();
		$argv = $_SERVER['argv'];
		$_SERVER = $request->getServer();
		$_SERVER['argv'] = $argv;
		$_FILES = $request->getFiles();

		$_SERVER['HTTP_HOST'] = str_replace('localhost', $this->url, $_SERVER['HTTP_HOST']);
		$_SERVER['SERVER_NAME'] = str_replace('localhost', $this->url, $_SERVER['HTTP_HOST']);

		$_SERVER['HTTP_HOST'] = str_replace('http://', '', $_SERVER['HTTP_HOST']);
		$_SERVER['HTTP_HOST'] = str_replace('https://', '', $_SERVER['HTTP_HOST']);

		$_SERVER['REQUEST_METHOD'] = $method = strtoupper($request->getMethod());

		if (Strings::startsWith($this->url, 'https')) {
			$_SERVER['HTTPS'] = 'https';
		}

		$_SERVER['REQUEST_URI'] = str_replace('http://localhost', '', $request->getUri());
		$_SERVER['REQUEST_URI'] = str_replace($this->url, '', $_SERVER['REQUEST_URI']);
		$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
		$_SERVER['SCRIPT_NAME'] = '/index.php';

		$_POST = $_GET = [];
		if ($method === 'HEAD' || $method === 'GET') {
			$_GET = $request->getParameters();
		} else {
			$_POST = $request->getParameters();
		}
	}




	public function getUrl($name, $params)
	{
		$httpRequest = $this->container->getByType(IRequest::class);

		$request = new \Nette\Application\Request($name, NULL, $params);

		$refUrl = clone $httpRequest->getUrl();
		$constructedUrl = $this->application->getRouter()->constructUrl($request, $refUrl);

		$newUrl = new Url($constructedUrl);

		if (class_exists('Kdyby\Console\HttpRequestFactory')) {
			/** @var \Kdyby\Console\HttpRequestFactory $requestFactory */
			$requestFactory = $this->container->getByType('Nette\Http\RequestFactory');
			$requestFactory->setFakeRequestUrl($constructedUrl);
		}

		return $newUrl->getPath();
	}

}
