<?php

namespace Atreo\Codeception\Module;

use Codeception\Lib\Framework;
use Codeception\Step;
use Codeception\TestCase;
use Nette\DI\Container;
use Nette\DI\MissingServiceException;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class NetteContainer extends Framework
{

	/**
	 * @var array
	 */
	protected $config = [
		'bootstrap' => 'app/bootstrap.php'
	];

	/**
	 * @var Container
	 */
	protected $container;



	public function _initialize()
	{
		parent::_initialize();
 		$this->container = $this->getContainer();
	}



	/**
	 * @return Container
	 */
	private function getContainer()
	{
		$container = require codecept_root_dir() . $this->config['bootstrap'];
		return $container;
	}



	/**
	 * @param string $service
	 * @return mixed
	 */
	public function grabService($service)
	{
		try {
			return $this->container->getByType($service);
		} catch (MissingServiceException $e) {
			$this->fail($e->getMessage());
		}
	}



	public function debugContent()
	{
		$this->debugSection('Content', $this->client->getInternalResponse()->getContent());
	}

}
