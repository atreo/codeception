Atreo/Codeception
===========================


Installation
------------

```sh
$ composer require atreo/codeception
```

Extension configuration:

```
codeception: Atreo\Codeception\Di\CodeceptionExtension
```

Update your bootstrap.php like in example. Probably just add:

```
$configurator->addParameters([
	'appDir' => __DIR__,
	'wwwDir' => __DIR__ . '/../www'
]);
```

And then copy example codeception.yml and tests folder to your project root.

Now you are ready to run codeception:

```sh
$ php vendor/bin/codecept run
```

If you are using kdyby/events you will probably have to update composer.json:


```
"require": { "kdyby/events": "@dev" },
"repositories": [
	{ "type": "git", "url": "https://github.com/AtreoCZ/Events.git" }
]
```